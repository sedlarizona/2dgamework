package me.sedlar.gamework.util;

import java.awt.Graphics2D;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public interface Renderable {

    void render(Graphics2D g);
}
package me.sedlar.gamework;

import me.sedlar.gamework.ui.GameApplet;

import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.WindowConstants;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class GameFrame extends JFrame {

    private final Timer timer;
    private int fps = 50;

    public GameFrame(String title, GameApplet applet) {
        super(title);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        add(applet);
        pack();
        setLocationRelativeTo(null);
        timer = new Timer(1000 / fps, e -> applet.repaint());
        timer.start();
    }

    public void setFPS(int fps) {
        this.fps = fps;
        timer.setDelay(1000 / fps);
    }
}
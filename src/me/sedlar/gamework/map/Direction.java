package me.sedlar.gamework.map;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public enum Direction {

    NORTH,
    EAST,
    SOUTH,
    WEST
}
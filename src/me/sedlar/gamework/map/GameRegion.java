package me.sedlar.gamework.map;

import me.sedlar.gamework.map.entity.Entity;
import me.sedlar.gamework.util.Renderable;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class GameRegion implements Renderable {

    public final Tile[] tiles;
    public final List<Entity> entities = new ArrayList<>();

    private GameMap map;

    public GameRegion(Tile... tiles) {
        this.tiles = tiles;
        for (Tile tile : tiles) {
            tile.setRegion(this);
        }
        Arrays.sort(tiles, (a, b) -> {
            int compare = a.row() - b.row();
            if (compare == 0) {
                compare = a.column() - b.column();
            }
            return compare;
        });
    }

    public GameMap map() {
        return map;
    }

    public void setMap(GameMap map) {
        this.map = map;
    }

    public double distanceTo(Tile tile) {
        double closest = Double.MAX_VALUE;
        for (Tile t : tiles) {
            double dist = t.distanceTo(tile);
            if (dist < closest)
                closest = dist;
        }
        return closest;
    }

    public void outline(Color color) {
        for (Tile tile : tiles) {
            tile.setOutline(color);
        }
    }

    public void removeOutline() {
        for (Tile tile : tiles) {
            tile.setOutline(null);
        }
    }

    public static GameRegion generate(int startRow, int startColumn, int tileSize, int regionSize, Color color) {
        List<Tile> tiles = new ArrayList<>();
        for (int c = 0; c < regionSize / tileSize; c++) {
            for (int r = 0; r < regionSize / tileSize; r++) {
                tiles.add(new Tile(startRow + r, startColumn + c, tileSize, color));
            }
        }
        return new GameRegion(tiles.toArray(new Tile[tiles.size()]));
    }

    @Override
    public void render(Graphics2D g) {
        for (Tile tile : tiles) {
            tile.render(g);
        }
        for (Entity entity : entities) {
            entity.render(g);
        }
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeInt(tiles.length);
        for (Tile tile : tiles)
            tile.write(out);
    }

    public static GameRegion read(DataInputStream in) throws IOException {
        int tileCount = in.readInt();
        List<Tile> tiles = new ArrayList<>();
        for (int i = 0; i < tileCount; i++)
            tiles.add(Tile.read(in));
        return new GameRegion(tiles.toArray(new Tile[tiles.size()]));
    }
}
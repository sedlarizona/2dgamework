package me.sedlar.gamework.map.entity;

import me.sedlar.gamework.map.Direction;
import me.sedlar.gamework.map.Tile;
import me.sedlar.util.Time;
import me.sedlar.util.io.ResourceLoader;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class Player extends Entity {

    public static ResourceLoader SPRITE_LOADER = new ResourceLoader("/com/pokeclone/res/images/player/");

    public static BufferedImage loadSprite(String spriteId, String spriteName) {
        return SPRITE_LOADER.imageFor(String.format("%s/%s", spriteId, spriteName));
    }

    private BufferedImage sprite;
    public final BufferedImage spriteFront, spriteFrontWalk;
    public final BufferedImage spriteBack, spriteBackWalk;
    public final BufferedImage spriteLeft, spriteLeftWalk;
    public final BufferedImage spriteRight, spriteRightWalk;

    public Player(String spriteId, Tile location) {
        super(location);
        sprite = spriteFront = loadSprite(spriteId, "sprite-front.png");
        spriteFrontWalk = loadSprite(spriteId, "sprite-front-walk.png");
        spriteBack = loadSprite(spriteId, "sprite-back.png");
        spriteBackWalk = loadSprite(spriteId, "sprite-back-walk.png");
        spriteLeft = loadSprite(spriteId, "sprite-left.png");
        spriteLeftWalk = loadSprite(spriteId, "sprite-left-walk.png");
        spriteRight = loadSprite(spriteId, "sprite-right.png");
        spriteRightWalk = loadSprite(spriteId, "sprite-right-walk.png");
    }

    public void setSprite(BufferedImage sprite) {
        this.sprite = sprite;
    }

    public void face(Direction direction) {
        BufferedImage sprite;
        if (direction == Direction.NORTH) {
            sprite = spriteBack;
        } else if (direction == Direction.SOUTH) {
            sprite = spriteFront;
        } else if (direction == Direction.EAST) {
            sprite = spriteRight;
        } else {
            sprite = spriteLeft;
        }
        this.sprite = sprite;
    }

    public void walk(Direction direction, Tile target, Runnable onFinish) {
        new Thread(() -> {
            for (int i = 0; i < location.size(); i++) {
                Time.sleep(5);
                if (direction == Direction.NORTH) {
                    drawOffsetY--;
                } else if (direction == Direction.SOUTH) {
                    drawOffsetY++;
                } else if (direction == Direction.EAST) {
                    drawOffsetX++;
                } else if (direction == Direction.WEST) {
                    drawOffsetX--;
                }
                Time.sleep(5);
            }
            location.set(target);
            drawOffsetX = 0;
            drawOffsetY = 0;
            onFinish.run();
        }).start();
        new Thread(() -> {
            BufferedImage stationary, walking;
            if (direction == Direction.NORTH) {
                stationary = spriteBack;
                walking = spriteBackWalk;
            } else if (direction == Direction.SOUTH) {
                stationary = spriteFront;
                walking = spriteFrontWalk;
            } else if (direction == Direction.EAST) {
                stationary = spriteRight;
                walking = spriteRightWalk;
            } else {
                stationary = spriteLeft;
                walking = spriteLeftWalk;
            }
            sprite = stationary;
            Time.sleep(100);
            sprite = walking;
            Time.sleep(100);
            sprite = stationary;
        }).start();
    }

    @Override
    public void renderAt(Graphics2D g, int x, int y) {
        g.setColor(Color.RED);
        g.drawImage(sprite, x + drawOffsetX(), y + drawOffsetY(), null);
    }
}
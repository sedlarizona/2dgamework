package me.sedlar.gamework.map.entity;

import me.sedlar.gamework.map.Tile;
import me.sedlar.gamework.util.Renderable;

import java.awt.Graphics2D;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public abstract class Entity implements Renderable {

    public final Tile location;

    protected int drawOffsetX, drawOffsetY;

    public Entity(Tile location) {
        this.location = location;
    }

    public int drawOffsetX() {
        return drawOffsetX;
    }

    public void setDrawOffsetX(int drawOffsetX) {
        this.drawOffsetX = drawOffsetX;
    }

    public int drawOffsetY() {
        return drawOffsetY;
    }

    public void setDrawOffsetY(int drawOffsetY) {
        this.drawOffsetY = drawOffsetY;
    }

    @Override
    public void render(Graphics2D g) {
        int x = location.column() * location.size();
        int y = location.row() * location.size();
        renderAt(g, x, y);
    }

    public abstract void renderAt(Graphics2D g, int x, int y);
}
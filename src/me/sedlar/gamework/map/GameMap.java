package me.sedlar.gamework.map;

import me.sedlar.gamework.map.entity.Entity;

import java.awt.Graphics2D;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class GameMap {

    public final int regionSize;
    public final GameRegion[] regions;

    public GameMap(int regionSize, GameRegion... regions) {
        this.regionSize = regionSize;
        this.regions = regions;
        for (GameRegion region : regions) {
            region.setMap(this);
        }
    }

    public Tile tileFor(int row, int column) {
        for (GameRegion region : regions) {
            for (Tile tile : region.tiles) {
                if (tile.row() == row && tile.column() == column) {
                    return tile;
                }
            }
        }
        return null;
    }

    public List<Entity> entitiesInTileRegion(int row, int column) {
        Tile tile = tileFor(row, column);
        if (tile != null) {
            GameRegion region = tile.region();
            return region.entities;
        }
        return null;
    }

    public List<Entity> entitiesAt(int row, int column) {
        List<Entity> entities = new ArrayList<>();
        Tile tile = tileFor(row, column);
        if (tile != null) {
            GameRegion region = tile.region();
            entities.addAll(region.entities.stream().filter(
                    entity -> entity.location.row() == row && entity.location.column() == column
            ).collect(Collectors.toList()));
        }
        return entities;
    }

    public void render(Graphics2D g, Tile around, int maxDist) {
        for (GameRegion region : regions) {
            if (region.distanceTo(around) <= maxDist)
                region.render(g);
        }
    }

    public Tile tileAtMouse(Tile player, int mouseX, int mouseY, int appletWidth, int appletHeight) {
        if (mouseX == 0 || mouseY == 0)
            return null;
        int xOff = (appletWidth / 2) / player.size();
        int yOff = (appletHeight / 2) / player.size();
        int topLeftRow = player.row() - xOff;
        int topLeftColumn = player.column() - yOff;
        for (int r = 0; r < (appletWidth / player.size()); r++) {
            for (int c = 0; c < (appletHeight / player.size()); c++) {
                int x = (c * player.size());
                int y = (r * player.size());
                if (mouseX >= x && mouseX <= (x + player.size()) && mouseY >= y && mouseY <= (y + player.size())) {
                    return tileFor(topLeftRow + r, topLeftColumn + c);
                }
            }
        }
        return null;
    }

    public void writeTileData(DataOutputStream out) throws Exception {
        List<Tile> changed = new ArrayList<>();
        for (GameRegion region : regions) {
            for (Tile tile : region.tiles) {
                if (!tile.defaulted())
                    changed.add(tile);
            }
        }
        out.writeInt(changed.size());
        for (Tile tile : changed)
            tile.write(out);
    }

    public int readTileData(DataInputStream in) throws Exception {
        int tileCount = in.readInt();
        for (int i = 0; i < tileCount; i++) {
            Tile read = Tile.read(in);
            Tile real = tileFor(read.row(), read.column());
            if (real != null) {
                real.setSize(read.size());
                real.setColor(read.color());
                real.setTexture(read.texturePath());
                real.setGroundTexture(read.groundTexturePath());
                real.setWalkable(read.walkable());
            } else {
                System.out.println("Invalid map data tile at " + read.row() + ", " + read.column());
            }
        }
        return tileCount;
    }
}
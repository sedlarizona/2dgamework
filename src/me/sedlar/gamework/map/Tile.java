package me.sedlar.gamework.map;

import me.sedlar.gamework.util.Renderable;
import me.sedlar.util.io.ResourceLoader;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class Tile implements Renderable {
    
    private static final String NULL = "null";

    private int size;
    private int row, column;
    private Color color = Color.WHITE, outline;
    private BufferedImage texture, groundTexture;
    private String texturePath, groundTexturePath;
    private boolean walkable = true;

    private GameRegion region;

    public Tile(int row, int column, int size) {
        this(row, column, size, null, null, null, true);
    }

    public Tile(int row, int column, int size, boolean walkable) {
        this(row, column, size, null, null, null, walkable);
    }

    public Tile(int row, int column, int size, Color color) {
        this(row, column, size, color, null, null, true);
    }

    public Tile(int row, int column, int size, Color color, boolean walkable) {
        this(row, column, size, color, null, null, walkable);
    }

    public Tile(int row, int column, int size, String texture) {
        this(row, column, size, null, texture, null, true);
    }

    public Tile(int row, int column, int size, String texture, boolean walkable) {
        this(row, column, size, null, texture, null, walkable);
    }

    public Tile(int row, int column, int size, Color color, String texture) {
        this(row, column, size, color, texture, null, true);
    }

    public Tile(int row, int column, int size, Color color, String texture, boolean walkable) {
        this(row, column, size, color, texture, null, walkable);
    }

    public Tile(int row, int column, int size, String texture, String groundTexture) {
        this(row, column, size, null, texture, groundTexture, true);
    }

    public Tile(int row, int column, int size, String texture, String groundTexture, boolean walkable) {
        this(row, column, size, null, texture, groundTexture, walkable);
    }

    public Tile(int row, int column, int size, Color color, String texture, String groundTexture) {
        this(row, column, size, color, texture, groundTexture, true);
    }

    public Tile(int row, int column, int size, Color color, String texture, String groundTexture, boolean walkable) {
        this.row = row;
        this.column = column;
        this.size = size;
        this.color = color;
        setTexture(texture);
        setGroundTexture(groundTexture);
        this.walkable = walkable;
    }

    public int row() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int column() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int size() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Color color() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setColor(int rgb) {
        color = new Color(rgb);
    }

    public Color outline() {
        return outline;
    }

    public void setOutline(Color outline) {
        this.outline = outline;
    }

    public void setOutline(int outline) {
        this.outline = new Color(outline);
    }
    
    public String texturePath() {
        return texturePath;
    }

    public BufferedImage texture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texturePath = texture;
        if (texture == null || texture.equals(NULL)) {
            this.texture = null;
            return;
        }
        try {
            this.texture = ResourceLoader.OBJECT_TEXTURES.imageFor(texture);
        } catch (Exception e) {
            this.texture = null;
        }
    }

    public String groundTexturePath() {
        return groundTexturePath;
    }

    public BufferedImage groundTexture() {
        return groundTexture;
    }

    public void setGroundTexture(String groundTexture) {
        this.groundTexturePath = groundTexture;
        if (groundTexture == null || groundTexture.equals(NULL)) {
            this.groundTexture = null;
            return;
        }
        try {
            this.groundTexture = ResourceLoader.GROUND_TEXTURES.imageFor(groundTexture);
        } catch (Exception e) {
            this.groundTexture = null;
        }
    }

    public boolean walkable() {
        if (region != null && region.map() != null) {
            if (!region.map().entitiesAt(row, column).isEmpty()) {
                return false;
            }
        }
        return walkable;
    }

    public void setWalkable(boolean walkable) {
        this.walkable = walkable;
    }

    public GameRegion region() {
        return region;
    }

    public void setRegion(GameRegion region) {
        this.region = region;
    }

    public void derive(int x, int y) {
        row = row + x;
        column = column + y;
    }

    public void set(Tile tile) {
        row = tile.row();
        column = tile.column();
    }

    public double distanceTo(Tile tile) {
        return Math.sqrt((column - tile.column) * (column - tile.column) + (row - tile.row) * (row - tile.row));
    }

    @Override
    public void render(Graphics2D g) {
        int x = column * size;
        int y = row * size;
        renderAt(g, x, y);
    }

    public void renderAt(Graphics2D g, int x, int y) {
        if (color != null) {
            g.setColor(color);
            g.fillRect(x, y, size, size);
        }
        if (groundTexture != null) {
            Shape clip = g.getClip();
            g.clipRect(x, y, size, size);
            g.drawImage(groundTexture, x, y, null);
            g.setClip(clip);
        }
        if (texture != null) {
            Shape clip = g.getClip();
            g.clipRect(x, y, size, size);
            g.drawImage(texture, x, y, null);
            g.setClip(clip);
        }
        if (outline != null) {
            g.setColor(outline);
            g.drawRect(x, y, size - 1, size - 1);
        }
    }

    public boolean defaulted() {
        return color.equals(Color.WHITE) && (texturePath == null || texturePath.equals(NULL)) &&
                (groundTexturePath == null || groundTexturePath.equals(NULL)) && walkable;
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeInt(row);
        out.writeInt(column);
        out.writeInt(size);
        out.writeInt(color.getRGB());
        out.writeUTF(texturePath != null ? texturePath : NULL);
        out.writeUTF(groundTexturePath != null ? groundTexturePath : NULL);
        out.writeBoolean(walkable);
    }

    public static Tile read(DataInputStream in) throws IOException {
        int row = in.readInt();
        int column = in.readInt();
        int size = in.readInt();
        int color = in.readInt();
        String texture = in.readUTF();
        String groundTexture = in.readUTF();
        boolean walkable = in.readBoolean();
        return new Tile(row, column, size, new Color(color), texture, groundTexture, walkable);
    }
}
package me.sedlar.gamework.ui;

import me.sedlar.gamework.util.Renderable;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public abstract class GameApplet extends Applet implements Renderable {

    private BufferedImage image;
    private BufferedImage clear;

    @Override
    public void update(Graphics g) {
        if (image == null || image.getWidth(null) != getWidth() || image.getHeight(null) != getHeight()) {
            image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            clear = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
        }
        image.setData(clear.getData());
        Graphics graphics = image.createGraphics();
        graphics.setColor(getBackground());
        graphics.fillRect(0, 0, getWidth(), getHeight());
        graphics.setColor(getForeground());
        paint(graphics);
        graphics.dispose();
        g.drawImage(image, 0, 0, this);
        g.dispose();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        render((Graphics2D) g);
    }
}
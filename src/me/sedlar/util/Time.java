package me.sedlar.util;

public class Time {

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {
        }
    }

    public static long toMillis(long nanos) {
        return nanos / 1000000;
    }

    public static long toNanos(long millis) {
        return millis * 1000000;
    }

    public static long millis() {
        return toMillis(System.nanoTime());
    }
}
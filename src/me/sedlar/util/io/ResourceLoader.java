package me.sedlar.util.io;

import javax.imageio.ImageIO;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author Tyler Sedlar
 * @since 11/1/14
 */
public class ResourceLoader {

    public static final ResourceLoader GROUND_TEXTURES = new ResourceLoader("/");
    public static final ResourceLoader OBJECT_TEXTURES = new ResourceLoader("/");

    private String dir;
    private final Map<String, BufferedImage> imageCache = new HashMap<>();
    private final Map<String, Font> fontCache = new HashMap<>();

    public ResourceLoader(String dir) {
        setDirectory(dir);
    }

    public String directory() {
        return dir;
    }

    public void setDirectory(String dir) {
        if (!dir.endsWith("/")) {
            dir += "/";
        }
        this.dir = dir;
    }

    public Set<File> files() {
        Set<File> files = new TreeSet<>((a, b) -> a.getName().compareTo(b.getName()));
        try {
            URL stream = getURL("/");
            File[] fileList = Paths.get(stream.toURI()).toFile().listFiles();
            if (fileList != null && fileList.length > 0)
                Collections.addAll(files, fileList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return files;
    }

    public File fileFor(String name) {
        try {
            URL stream = getURL("/");
            File file = Paths.get(stream.toURI()).toFile();
            return new File(file, name);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean hasFile(String name) {
        return fileFor(name).exists();
    }

    /**
     * Gets the InputStream from the given path.
     *
     * @param path The path to get from.
     * @return The InputStream from the given path.
     */
    public InputStream getStream(String path) {
        return ResourceLoader.class.getResourceAsStream(dir + path);
    }

    public URL getURL(String path) {
        return ResourceLoader.class.getResource(dir + path);
    }

    /**
     * Gets the raw content from the given path.
     *
     * @param path The path to getStream from.
     * @return The raw content from the given path.
     */
    public byte[] binaryFor(String path) {
        return Streams.binary(getStream(path));
    }

    /**
     * Gets the image from the given path.
     *
     * @param img The image to getStream.
     * @return The image from the given path.
     */
    public BufferedImage imageFor(String img) {
        if (imageCache.containsKey(img))
            return imageCache.get(img);
        try (InputStream in = getStream(img)) {
            BufferedImage image = ImageIO.read(in);
            imageCache.put(img, image);
            return image;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Gets the font from the given path.
     *
     * @param fontName The font to getStream.
     * @return The font from the given path.
     */
    public Font fontFor(String fontName) {
        return fontFor(fontName, Font.TRUETYPE_FONT, 12F);
    }

    /**
     * Gets the font from the given path.
     *
     * @param fontName The font to getStream.
     * @param fontType The type of font.
     * @return The font from the given path.
     */
    public Font fontFor(String fontName, int fontType) {
        return fontFor(fontName, fontType, 12F);
    }

    /**
     * Gets the font from the given path.
     *
     * @param fontName The font to getStream.
     * @param fontType The type of font.
     * @param size The size of the font.
     * @return The font from the given path.
     */
    public Font fontFor(String fontName, int fontType, float size) {
        if (!fontCache.containsKey(fontName)) {
            try (InputStream in = getStream(fontName)) {
                Font font = Font.createFont(fontType, in).deriveFont(size);
                fontCache.put(fontName, font);
            } catch (IOException | FontFormatException e) {
                return null;
            }
        }
        return fontCache.get(fontName);
    }
}
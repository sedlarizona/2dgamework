package com.pokeclone.ui;

import me.sedlar.gamework.map.Tile;
import me.sedlar.util.io.ResourceLoader;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class TextureModifier extends JDialog {

    private Tile tile;
    private BufferedImage image;
    private final JComboBox<String> groundTexture;
    private final JComboBox<String> objectTexture;
    private final JCheckBox walkable;
    private final JCheckBox drag;
    private final Map<String, BufferedImage> images = new HashMap<>(15);

    public TextureModifier() {
        setTitle("Texture Modifier");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JPanel panel = new JPanel(new FlowLayout());
        panel.setPreferredSize(new Dimension(220, 125));
        JLabel groundLabel = new JLabel("Ground:");
        groundLabel.setPreferredSize(new Dimension(75, 25));
        groundLabel.setBorder(new EmptyBorder(0, 5, 0, 0));
        panel.add(groundLabel);
        List<String> groundTextures = new LinkedList<>();
        groundTextures.add("None");
        for (File file : ResourceLoader.GROUND_TEXTURES.files()) {
            groundTextures.add(file.getName());
            images.put(file.getName(), ResourceLoader.GROUND_TEXTURES.imageFor(file.getName()));
        }
        this.groundTexture = new JComboBox<>(groundTextures.toArray(new String[groundTextures.size()]));
        groundTexture.setRenderer(new ComboRenderer());
        groundTexture.setPreferredSize(new Dimension(125, 25));
        panel.add(groundTexture);
        JLabel objectLabel = new JLabel("Object:");
        objectLabel.setPreferredSize(new Dimension(75, 25));
        objectLabel.setBorder(new EmptyBorder(0, 5, 0, 0));
        panel.add(objectLabel);
        List<String> objectTextures = new LinkedList<>();
        objectTextures.add("None");
        for (File file : ResourceLoader.OBJECT_TEXTURES.files()) {
            objectTextures.add(file.getName());
            images.put(file.getName(), ResourceLoader.OBJECT_TEXTURES.imageFor(file.getName()));
        }
        this.objectTexture = new JComboBox<>(objectTextures.toArray(new String[objectTextures.size()]));
        objectTexture.setRenderer(new ComboRenderer());
        objectTexture.setPreferredSize(new Dimension(125, 25));
        panel.add(objectTexture);
        this.walkable = new JCheckBox("Walkable");
        walkable.setPreferredSize(new Dimension(80, 25));
        panel.add(walkable);
        this.drag = new JCheckBox("Drag");
        drag.setPreferredSize(new Dimension(80, 25));
        panel.add(drag);
        JButton set = new JButton("Set");
        set.setPreferredSize(new Dimension(210, 25));
        set.addActionListener(e -> apply(tile));
        panel.add(set);
        add(panel);
        pack();
        setLocationRelativeTo(null);
    }

    private class ComboRenderer extends DefaultListCellRenderer {

        public ComboRenderer() {
            setOpaque(true);
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(CENTER);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
                                                      boolean cellHasFocus) {
            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            BufferedImage image = images.get(value.toString());
            if (image != null) {
                ImageIcon icon = new ImageIcon(image);
                label.setIcon(icon);
            }
            return label;
        }
    }

    public boolean draggable() {
        return drag.isEnabled();
    }

    public void setTile(Tile tile) {
        this.tile = tile;
        if (tile.groundTexturePath() != null) {
            groundTexture.setSelectedItem(tile.groundTexturePath());
        }
        if (tile.texturePath() != null) {
            objectTexture.setSelectedItem(tile.texturePath());
        }
        walkable.setSelected(tile.walkable());
    }

    public void apply(Tile tile) {
        tile.setTexture((String) objectTexture.getSelectedItem());
        tile.setGroundTexture((String) groundTexture.getSelectedItem());
        tile.setWalkable(walkable.isSelected());
        tile.setColor(Color.WHITE); // TODO set teh refried bean color
    }

    public static void main(String... args) throws Exception {
        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
                e.printStackTrace();
            }
            new TextureModifier().setVisible(true);
        });
    }
}
package com.pokeclone;

import com.pokeclone.ui.TextureModifier;
import me.sedlar.gamework.GameFrame;
import me.sedlar.gamework.map.Direction;
import me.sedlar.gamework.map.GameMap;
import me.sedlar.gamework.map.GameRegion;
import me.sedlar.gamework.map.Tile;
import me.sedlar.gamework.map.entity.Player;
import me.sedlar.gamework.ui.GameApplet;
import me.sedlar.util.io.ResourceLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tyler Sedlar
 * @since 7/12/2015
 */
public class PokeClone {

    private static final boolean DEV = true;

    public static final int TILE_SIZE = 20;
    public static final String MAP_FILE = "./map.dat";

    public final String title;
    public final GameMap map;
    public final GameApplet applet;
    public final GameFrame frame;
    private final TextureModifier modifier;

    private final Player player = new Player("black", new Tile(0, 0, TILE_SIZE));

    private Tile hoveredTile, clickedTile;
    private boolean drawOutlines = false;

    private final List<Integer> list = new ArrayList<>(4);
    private boolean walking;

    public PokeClone() {
        title = "PokeClone";
        map = createMap();
        setCurrentRegion();
        applet = new GameApplet() {
            public void render(Graphics2D g) {
                handleWalking();
                setCurrentRegion();
                if (DEV) {
                    for (GameRegion region : map.regions)
                        region.outline(drawOutlines ? Color.BLACK : null);
                    if (hoveredTile != null) {
                        Tile tile = map.tileFor(hoveredTile.row(), hoveredTile.column());
                        if (tile != null)
                            tile.setOutline(drawOutlines ? Color.RED : null);
                    }
                    if (clickedTile != null) {
                        Tile tile = map.tileFor(clickedTile.row(), clickedTile.column());
                        if (tile != null)
                            tile.setOutline(drawOutlines ? Color.RED : null);
                    }
                }
                double camX = player.location.column() * TILE_SIZE + player.drawOffsetX() - applet.getWidth() / 2;
                double camY = player.location.row() * TILE_SIZE + player.drawOffsetY() - applet.getHeight() / 2;
                g.translate(-camX, -camY);
                map.render(g, player.location, map.regionSize / TILE_SIZE);
                g.translate(camX, camY);
            }
        };
        applet.setPreferredSize(new Dimension(400, 400));
        applet.setBackground(Color.BLACK);
        frame = new GameFrame(title, applet);
        frame.addWindowFocusListener(new WindowAdapter() {
            public void windowLostFocus(WindowEvent e) {
                list.clear();
            }
        });
        if (DEV) {
            modifier = new TextureModifier();
            applet.addMouseMotionListener(new MouseMotionAdapter() {
                public void mouseMoved(MouseEvent e) {
                    if (hoveredTile != null && hoveredTile != clickedTile) {
                        hoveredTile.setOutline(Color.BLACK);
                    }
                    hoveredTile = map.tileAtMouse(player.location, e.getX(), e.getY(), applet.getWidth(), applet.getHeight());
                }

                public void mouseDragged(MouseEvent e) {
                    if (!modifier.draggable() || !modifier.isVisible()) {
                        return;
                    }
                    Tile current = map.tileAtMouse(player.location, e.getX(), e.getY(), applet.getWidth(), applet.getHeight());
                    if (current != null) {
                        modifier.apply(current);
                    }
                }
            });
            applet.addMouseListener(new MouseAdapter() {
                public void mouseReleased(MouseEvent e) {
                    if (hoveredTile != null) {
                        if (e.getButton() == MouseEvent.BUTTON1) {
                            player.location.set(hoveredTile);
                        } else {
                            if (clickedTile != null)
                                clickedTile.setOutline(null);
                            clickedTile = hoveredTile;
                            modifier.setVisible(true);
                            modifier.setTile(clickedTile);
                        }
                    }
                }
            });
            frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    try (DataOutputStream out = new DataOutputStream(new FileOutputStream(MAP_FILE))) {
                        map.writeTileData(out);
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
                }
            });
        }
        applet.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int code = e.getKeyCode();
                if (e.isControlDown() && code == KeyEvent.VK_O) {
                    drawOutlines = !drawOutlines;
                } else {
                    if (direction(code) != null && !list.contains(code) && (list.isEmpty() || list.get(0) != code)) {
                        list.add(0, code);
                    }
                }
            }

            public void keyReleased(KeyEvent e) {
                int code = e.getKeyCode();
                if (direction(code) != null) {
                    list.remove((Integer) code);
                }
            }
        });
        applet.requestFocus();
    }

    public void setCurrentRegion() {
        GameRegion region = region();
        if (region != null) {
            if (player.location.region() != null) {
                player.location.region().entities.remove(player);
            }
            player.location.setRegion(region);
            region.entities.add(player);
        }
    }

    private GameRegion region() {
        Tile self = map.tileFor(player.location.row(), player.location.column());
        return self != null ? self.region() : null;
    }

    private GameMap createMap() {
        ResourceLoader.OBJECT_TEXTURES.setDirectory("/com/pokeclone/res/images/textures/objects/");
        ResourceLoader.GROUND_TEXTURES.setDirectory("/com/pokeclone/res/images/textures/ground/");
        File mapFile = new File(MAP_FILE);
        int regionCount = 20;
        int regionsAcross = 10;
        int regionTileSize = 20;
        int regionSize = TILE_SIZE * regionTileSize;
        List<GameRegion> regions = new LinkedList<>();
        int row = 0, column = 0;
        for (int i = 0; i < regionCount; i++) {
            for (int j = 0; j < regionsAcross; j++) {
                regions.add(GameRegion.generate(row, column, TILE_SIZE, regionSize, Color.WHITE));
                column += (regionSize / TILE_SIZE);
            }
            column = 0;
            row += (regionSize / TILE_SIZE);
        }
        GameMap map = new GameMap(regionSize, regions.toArray(new GameRegion[regions.size()]));
        if (mapFile.exists()) {
            try {
                try (DataInputStream in = new DataInputStream(new FileInputStream(mapFile))) {
                    int modified = map.readTileData(in);
                    System.out.println("tiles modified: " + modified);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    private Direction direction(int key) {
        if (key == KeyEvent.VK_W) {
            return Direction.NORTH;
        } else if (key == KeyEvent.VK_S) {
            return Direction.SOUTH;
        } else if (key == KeyEvent.VK_A) {
            return Direction.WEST;
        } else if (key == KeyEvent.VK_D) {
            return Direction.EAST;
        }
        return null;
    }

    private void handleWalking() {
        Tile target;
        Direction direction;
        if (!list.isEmpty()) {
            int key = list.get(0);
            if (key == KeyEvent.VK_W) {
                target = map.tileFor(player.location.row() - 1, player.location.column()); // y - 1
                direction = Direction.NORTH;
            } else if (key == KeyEvent.VK_S) {
                target = map.tileFor(player.location.row() + 1, player.location.column()); // y + 1
                direction = Direction.SOUTH;
            } else if (key == KeyEvent.VK_A) {
                target = map.tileFor(player.location.row(), player.location.column() - 1); // x - 1
                direction = Direction.WEST;
            } else if (key == KeyEvent.VK_D) {
                target = map.tileFor(player.location.row(), player.location.column() + 1); // x + 1
                direction = Direction.EAST;
            } else {
                target = null;
                direction = null;
            }
        } else {
            target = null;
            direction = null;
        }
        if (direction != null && !walking) {
            player.face(direction);
        }
        if (target != null && target.walkable() && !walking) {
            walking = true;
            player.walk(direction, target, () -> walking = false);
        }
    }

    public static void main(String... args) {
        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
                e.printStackTrace();
            }
            new PokeClone().frame.setVisible(true);
        });
    }
}